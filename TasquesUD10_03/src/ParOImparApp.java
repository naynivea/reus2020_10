import java.util.Scanner;

import dto.ParOImpar;


/**
 * @author Nayara Nivea Gomes Santos
 *
 */
public class ParOImparApp {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Scanner entrada = new Scanner(System.in);
		
		System.out.print("Insira un n�mero: ");
		int num = entrada.nextInt();
		
		ParOImpar parOimpar = new ParOImpar(num);
		parOimpar.esImparOpar();
	}

}
