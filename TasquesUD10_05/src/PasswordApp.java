import java.util.Scanner;

import dto.Password;

/**
 * 
 */

/**
 * @author Nayara Nivea Gomes Santos
 *
 */
public class PasswordApp {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Scanner entrada = new Scanner(System.in);
		
		System.out.println("Entrar cu�ntos Passwords quieres.");
		int tamano = entrada.nextInt();
		Password[] password = new Password[tamano];
		boolean[] esFuerte = new boolean[tamano];
		
		System.out.println("�Cu�l ser� la longitud de las contrase�as?.");
		int longitud = entrada.nextInt();
		
		for (int i = 0; i < password.length; i++) {
			try {
				password[i] = new Password(longitud);
				esFuerte[i] = password[i].esFuerte(); 
				
				System.out.println("Contrase�a: " + password[i].getContrasena() + " �Es fuerte? " + esFuerte[i]);
			} catch (Exception e) {
				System.out.println(e.getMessage());
			}
		}
	}

}
