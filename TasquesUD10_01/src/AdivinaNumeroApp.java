import views.Vista;


/**
 * @author Nayara Nivea Gomes Santos
 *
 */
public class AdivinaNumeroApp {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Vista jogo1 = new Vista();

		jogo1.mostrarResultado();
	}

}
