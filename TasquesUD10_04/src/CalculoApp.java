import java.util.Scanner;

import dto.Calculo;
import views.Vista;



/**
 * @author Nayara Nivea Gomes Santos
 *
 */
public class CalculoApp {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Calculo calculo = new Calculo();
		Vista vista = new Vista();
		Scanner entrada = new Scanner(System.in);
		
		System.out.println("Introduce el valor de x: ");
		double x = entrada.nextDouble();
		System.out.println("Introduce el valor de y: ");
		double y = entrada.nextDouble();
		
		try {
			calculo.sumar(x, y);
			calculo.restar(x, y);
			calculo.multiplicar(x, y);
			calculo.potencia(x, y);
			calculo.raizCuadrada(x);
			calculo.raizCubica(x);
			calculo.dividir(x, y);
		} catch (ArithmeticException e) {
			vista.mostrarResultado("Error");
		}
	}

}
