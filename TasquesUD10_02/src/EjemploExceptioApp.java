import dto.EjemploException;


/**
 * @author Nayara Nivea Gomes Santos
 *
 */
public class EjemploExceptioApp {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		EjemploException ej = new EjemploException();
		
		ej.capturarExcepcion();
	}

}
